#!/bin/bash

if [[ `whoami` != "root" ]]; then
	echo "Prosim pustit jako root."
	exit
fi

apt-get install vsftpd -y
echo "Nainstalovano VSFTPD."

sed -i '/chroot_local_user=YES/s/^#//g' /etc/vsftpd.conf
echo "Upraven zaznam. (1/5)"

sed -i '/local_enable=YES/s/^#//g' /etc/vsftpd.conf
echo "Upraven zaznam. (2/5)"

sed -i '/write_enable=YES/s/^#//g' /etc/vsftpd.conf
echo "Upraven zaznam. (3/5)"

sed -i '/pam_service_name=/s/vsftpd/ftp/g' /etc/vsftpd.conf
echo "Upraven zaznam. (4/5)"

sed -i '/anonymous_enable=YES/s/anonymous_enable=YES/#anonymous_enable=YES/g' /etc/vsftpd.conf
echo "Upraven zaznam. (5/5)"

echo "Jmeno uzivatele pro pristup: "
read uzivatel;

echo "Zmena hesla pro uzivatele: "
passwd "$uzivatel"

echo "Korenovy adresar pro uzivatele: "
read domovska;

uid=`cat /etc/passwd | cut -d : -f 3 | sort -n | awk '$1 > 1000 && $1 < 5000' | tail -1`
luid=$(($uid+1))

echo "$uzivatel:x:$luid:$luid:FTP uzivatel:$domovska:/sbin/nologin" >> /etc/passwd

service vsftpd restart
